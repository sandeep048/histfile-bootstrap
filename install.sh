#!/bin/zsh -e
#
# Histfile bootstrap shell script
# Author: Sandeep Pathivada
#
# This script will try and create a private repo in gitlab
# where all of your shell history can be archived.
#

if [ ! -n "$ZSH_VERSION" ]; then
    echo 'This setup requires zsh'
    exit 1
fi

if [ ! -d "~/.histfile" ]; then
    # Check if environment variables are set.
    if [[ -z $GITLAB_USER || -z $GITLAB_PRIVATE_TOKEN ]]; then
        echo 'Please export GITLAB_USER, GITLAB_PRIVATE_TOKEN variables and rerun this script'
        exit 1
    fi

    # Check if git bare repository is present.
    result=$(curl -s -o /dev/null -I -w "%{http_code}"  --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "https://gitlab.com/api/v3/projects/$GITLAB_USER%2Fhistfile")

    # If not create one in gitlab.
    if [ $result -ne 200 ]; then
        params="name=histfile&description=Git+repository+that+archives+shell+history.&builds_enabled=False&wiki_enabled=False&issues_enabled=False&public=False&merge_requests_enabled=False"
        histfile_repo=$(curl -s -o /dev/null --data $params --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" https://gitlab.com/api/v3/projects/)
    fi

    # Clone that repo
    git clone git@gitlab.com:$GITLAB_USER/histfile.git ~/.histfile

    # Copy dependencies, shell hooks ...
    if [ ! -d "~/.histfile/lib" ]; then
        cp -r lib ~/.histfile/
    fi

    # Append histfile hooks to zshrc
    if [ -f ~/.zshrc ]; then
        echo "source ~/.histfile/lib/histfile-hooks.sh" >>! ~/.zshrc  
    fi

    echo "All set. This directory can be safely removed now."
fi
