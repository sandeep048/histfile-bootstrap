Introduction
-----------------
This project allows a user to safely archive all shell history in
a private git repository. At the moment this script is configured to work Zsh
and [Gitlab.com](https://gitlab.com)

Features
------------
 - Alternative to bashhub with better control over storage.
 - Uses Git for private storage and Cron for sync.
 - Files are saved in a simple TSV format. Easy to view/load in a database and analyze.
 - Can be used across multiple machines. Each machine commits to
   its own folder so there will never be a merge conflict.

Setup
-----
 - Get Gitlab private token from [account](https://gitlab.com/profile/account) page.
 - export GITLAB_USER and GITLAB_PRIVATE_TOKEN variables.
 - Clone this repo.
 - ./install.sh

ToDo
-------
 - Build a history search trigger ~ctrl+b. (At the moment we can use grep/sift/ack/ag to manually search the archive)
 -  A nice usable web interface. Search from anywhere. 
 - [Enhancement] Transparent file encryption using a private token (Gitlab/Github/Bitbucket/...).
 - Configure cron sync frequency.
