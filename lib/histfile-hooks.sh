preexec() {
    TAINTED=1
    CMD=$1
    CREATED_AT=$(date +%s)
    SECONDS=0
}

precmd() {
    EXIT_STATUS=$?
    ELAPSED_SECS=$SECONDS
    if [ -z $TAINTED ]; then
        return
    fi
    # Get a uniquely generated session ID.
    if [ -z $SESSION_ID ]; then
        SESSION_ID=$(uuidgen | awk -F '-' '{print $1}')
    fi
    # OUTFILE="~/.histfile/"$(date +%Y-%m-%d)".hf"
    HIST_ENTRY="$SESSION_ID\t$CREATED_AT\t$ELAPSED_SECS\t$PWD\t$CMD\t$EXIT_STATUS\n"
    # exec printf $HIST_ENTRY >>! $OUTFILE
    # Manage concurrent access
    # while true; do nc -lU histfile.sock; done
    # ncat -lU histfile.sock
    printf $HIST_ENTRY | ncat -U ~/.histfile/.flock.sock
    # printf $HIST_ENTRY >>! ~/.histfile/2016-03-26.hf
    TAINTED=
}

